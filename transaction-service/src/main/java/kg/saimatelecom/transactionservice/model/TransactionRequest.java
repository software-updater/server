package kg.saimatelecom.transactionservice.model;

import kg.saimatelecom.transactionservice.enums.TransactionRequestStatus;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Table(name ="profile_changes_requests")
@Builder
@Setter
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TransactionRequest {
    @Id
    Long id;
    Timestamp createdAt;
    Timestamp updatedAt;
    boolean deleted;
    String sender;
    String receiver;
    BigDecimal amount;
    TransactionRequestStatus status;
}
